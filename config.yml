project_name: grap-12-16

postgres_image_name: postgres:16
postgres_container_name: grap-12-16-container-postgres-16
postgres_volume_name: grap-12-16-volume-postgres-16

postgres_extra_settings:
  shared_buffers: 4GB
  work_mem: 64MB
  maintenance_work_mem: 2GB
  effective_cache_size: 12GB
  wal_level: minimal
  fsync: off
  full_page_writes: off
  synchronous_commit: off
  max_wal_senders: 0

odoo_rpc_timeout: 3600
odoo_host_xmlrpc_port: 9069
odoo_default_company_2:
  country_code: FR
  phone: +33972323317
  email: contact@grap.coop

odoo_versions:
  - 12.0
  - 13.0
  - 14.0
  - 15.0
  - 16.0

odoo_version_settings:
  12.0:
    repo_url: https://gitlab.com/grap-rhone-alpes/grap-odoo-env
    repo_branch: 12.0
    repo_file_path: repos.yml
  13.0:
  14.0:
  15.0:
  16.0:
    repo_url: https://gitlab.com/grap-rhone-alpes/grap-odoo-env
    repo_branch: 16.0
    repo_file_path: repos.yml

migration_steps:
  - name: 1
    version: 12.0
    execution_context: regular
    complete_name: step_01__regular__12.0
    skip_update: True

  - name: 2
    version: 13.0
    execution_context: openupgrade
    complete_name: step_02__openupgrade__13.0

  - name: 3
    version: 14.0
    execution_context: openupgrade
    complete_name: step_03__openupgrade__14.0

  - name: 4
    version: 15.0
    execution_context: openupgrade
    complete_name: step_04__openupgrade__15.0

  - name: 5
    version: 16.0
    execution_context: openupgrade
    complete_name: step_05__openupgrade__16.0

  - name: 6
    version: 16.0
    execution_context: regular
    complete_name: step_06__regular__16.0

workload_settings:
  ignored_module_list:
    # ==================
    # <done>
    # ==================
    # ################################################################
    # désinstallé en production en V12 (MeP n°8)
    # ################################################################
    - product_analytic
    - account_check_deposit
    - product_replenishment_cost
    - account_fiscal_position_usage_group
    - stock_preparation_category

    # ################################################################
    # désinstallé en production en V12 (MeP n°9)
    # ################################################################
    - sale_mrp_link

    # ################################################################
    # A désinstaller en production en V12 (MeP n°X)
    # ################################################################
    - database_synchronization
    - product_restricted_type

    # ################################################################
    # désinstallé durant l'upgrade. (step 1, V12, post-migration.py)
    # ################################################################
    - pos_unavailable_product_friendly_error # Inutile en V16
    - pos_warning_exiting # Inutile en V16
    - web_no_bubble # Inutile en V16
    - web_switch_context_warning # Inutile en V16
    - multi_company_point_of_sale # Inutile en V16 (https://github.com/odoo/odoo/pull/183045)
    - web_base_url_force # On dirait qu'on a pas de soucis en V16. A voir si ça tient !


      # GRAP: on dégomme les company_wizard modules
    - company_wizard_account
    - company_wizard_base
    - company_wizard_product
    - fiscal_company_company_wizard_account
    - fiscal_company_company_wizard_base

    - pos_check_session_state # On reverra ça en V16
    - pos_default_empty_image # On reverra ça en V16
    - pos_disable_change_cashier # On reverra ça en V16
    - pos_prevent_double_closing # On reverra ça en V16
    - pos_timeout # On reverra ça en V16
    - pos_report_session_summary # On reverra ça en V16
    - pos_hide_empty_category # On reverra ça en V16
    - purchase_propagate_qty # On reverra ça en V16
    - web_group_by_percentage # On reverra ça en V16
    - web_searchbar_full_width # On reverra ça en V16

    # désinstallé durant l'upgrade. (step 2, V13, post-migration.py)
    # Extra note: account_bank_statement_import is odoo auto-installable.
    # So, it is reinstalled in V13. We so uninstall it again.
    # Then, since V14, the module is in OCA.
    - account_bank_statement_import

    # Set in step 2, because account_invoice_invoice2data depends on it
    - account_invoice_recompute_tax # Inutile en V16
      # Set in step 2, because technical_partner_access/multi_search_partner/joint_buying_base depends on it
    - name_search_reset_res_partner # Le _name_search de la V16 semble OK.

    # désinstallé durant l'upgrade. (step 3, V14, post-migration.py)
    # Extra note: mass_operation_abstract n'est plus une dépendance de
    # mass_editing à partir de la version 14.0
    - mass_operation_abstract

    # renamed into pos_sale_order_load in V14
    # https://github.com/OCA/OpenUpgrade/pull/4580
    # (then pos_sale_order_load is merged into pos_sale in V15)
    - pos_picking_load
    # merged into point_of_sale, since V15.
    # OK. https://github.com/OCA/OpenUpgrade/pull/4579
    - pos_order_line_note
    # merged into point_of_sale, since V15
    - pos_order_mgmt

    # Replaced by crm_tag_multi_company / crm_stage_multi_company / crm_lost_reason_multi_company in V16
    - multi_company_crm

    # ==================
    # </done>
    # ==================

    # ==================
    # <TODO>
    # ==================
    # To migrate
    - account_invoice_view_payment

    # To Investigate
    - product_sale_tax_price_included # Utile en V16 ?
    - product_price_history # It seems deprecated.

    # To Wait
    # Draft work : https://github.com/OCA/web/pull/2928 / https://github.com/OCA/web/pull/2873
    - web_tree_dynamic_colored_field

    # ==================
    # </TODO>
    # ==================

  # porting a module requires 45 minutes minimaly
  port_minimal_time: 45

  # a migration cost more for each version
  port_per_version: 15

  # Porting 120 lines of Python code costs 1 hour
  port_per_python_line_time: 0.5

  # Porting 120 lines of Javascript code costs 1 hour
  port_per_javascript_line_time: 0.5

  # Porting 10 lines of XML costs 1 minute
  port_per_xml_line_time: 0.10

  # Minimal time for Openupgrade PR
  open_upgrade_minimal_time: 10

  # time for a line of model in the openupgrade_analysis.txt
  openupgrade_model_line_time: 10

  # Time for a line of field in the openupgrade_analysis.txt
  openupgrade_field_line_time: 5

  # Time for a line of XML in the openupgrade_analysis.txt
  openupgrade_xml_line_time: 0.1
