#!/bin/bash

odoo-openupgrade-wizard -l DEBUG docker-build --versions 13.0,14.0,15.0
odoo-openupgrade-wizard -l DEBUG upgrade --database test_grap_12_16_012 --first-step=2
