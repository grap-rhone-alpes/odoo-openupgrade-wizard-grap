#!/bin/bash

# set -euxo pipefail
set -ex


# name of the files that comes from production
FILESTORE_ORIGINAL_NAME='caap_production__2025_01_31__03_20_01'

# Base name for the database
DB_ORIGINAL_NAME='caap_2025_01_31'

# name of the template database
DB_TEMPLATE_NAME=$DB_ORIGINAL_NAME'__template'

# name of the working database
DB_NAME=$DB_ORIGINAL_NAME'__try_12_fev_ab'

# name of the finally migrated database
DB_FINAL_NAME=$DB_NAME'__migrated_in_v16'

EXPORT_FOLDER='data_to_production/'$DB_FINAL_NAME'/'


# MANUAL: Get .dump and .tar.gz file from server
# MANUAL: copy files into date_from_production folder

# oow get-code

# oow docker-build

# Restore Data from production

# oow restoredb\
#   --database $DB_TEMPLATE_NAME\
#   --database-format=c\
#   --filestore-format=t\
#   --database-path='./data_from_production/'$FILESTORE_ORIGINAL_NAME'.dump'\
#   --filestore-path='./data_from_production/'$FILESTORE_ORIGINAL_NAME'.tar.gz'


oow copydb -s $DB_TEMPLATE_NAME -d $DB_NAME

# Step 1: Regular V12
oow upgrade -d $DB_NAME --first-step=1 --last-step=1
oow copydb -s $DB_NAME -d $DB_TEMPLATE_NAME'_PRE_V13_OU'

# Step 2: OpenUpgrade V13
oow upgrade -d $DB_NAME --first-step=2 --last-step=2
oow copydb -s $DB_NAME -d $DB_TEMPLATE_NAME'_PRE_V14_OU'

# Step 3: OpenUpgrade V14
oow upgrade -d $DB_NAME --first-step=3 --last-step=3
oow copydb -s $DB_NAME -d $DB_TEMPLATE_NAME'_PRE_V15_OU'

# Step 4: OpenUpgrade V15
oow upgrade -d $DB_NAME --first-step=4 --last-step=4
oow copydb -s $DB_NAME -d $DB_TEMPLATE_NAME'_PRE_V16_OU'

# Step 5: OpenUpgrade V16
oow upgrade -d $DB_NAME --first-step=5 --last-step=5
oow copydb -s $DB_NAME -d $DB_TEMPLATE_NAME'_PRE_V16'

# Step 6: Regular V16
oow upgrade -d $DB_NAME --first-step=6 --last-step=6
oow copydb -s $DB_NAME -d $DB_FINAL_NAME

# Export all migrated data in filestore
mkdir $EXPORT_FOLDER

oow dumpdb -d $DB_FINAL_NAME\
  --database-path=$EXPORT_FOLDER$DB_FINAL_NAME'.sql'\
  --database-format=p\
  --filestore-path=$EXPORT_FOLDER$DB_FINAL_NAME'.tar.gz'\
  --filestore-format=t

# Restore in local system
sudo /home/sylvain/grap_dev/server-script/env/bin/python /home/sylvain/grap_dev/server-script/odoo_restore_drop_obsolete.py



# TEMPORARY
DB_FINAL_NAME=$DB_NAME


# TODO

# ERREUR n°1 (V13)
# 2025-02-04 20:35:36,218 1 ERROR caap_2025_01_31__TRY_0003 post-migration: Error while recomputing draft invoice 19554: UserError("Incompatible companies on records:\n- '/ TVA achat 05,5% basé sur le prix HT' appartient à la société 'La Biscuiterie du Roc blanc' et 'Account' (account_id: '44566 TVA déductible sur autres biens et services') appartient à une autre société.", '')

# ERREUR n°2 (V13)

# 2025-02-04 20:40:36,117 1 DEBUG caap_2025_01_31__TRY_0003 OpenUpgrade: Deleting record account.account.tag#7
# 2025-02-04 20:40:36,122 1 ERROR caap_2025_01_31__TRY_0003 odoo.sql_db: bad query: DELETE FROM account_account_tag WHERE id IN (7)
# ERROR: update or delete on table "account_account_tag" violates foreign key constraint "account_account_tag_account_move_li_account_account_tag_id_fkey" on table "account_account_tag_account_move_line_rel"
# DETAIL:  Key (id)=(7) is still referenced from table "account_account_tag_account_move_line_rel".

# ERREUR n°3 (V13) EN COURS DE RESOLUTION

# 2025-02-04 20:47:16,452 8 ERROR caap_2025_01_31__TRY_0003 odoo.sql_db: bad query: DELETE FROM ir_model_fields WHERE id IN (12292, 10446, 10620, 10617, 10609, 8778, 10254, 7988, 11260, 10298, 12130, 12232, 11233, 11261, 12572, 10451, 10477, 12223, 12373, 10308, 10354, 8760, 8734, 12219, 11270, 10323, 12425, 10246, 8744, 10174, 12392, 10253, 10466, 10476, 12293, 10416, 12393, 12412, 12422, 12408, 12423, 12340, 12306, 10362, 12231, 10486, 12580, 11269)
# ERROR: update or delete on table "ir_model_fields" violates foreign key constraint "ir_actions_server_navigate_line_field_id_fkey" on table "ir_actions_server_navigate_line"
# DETAIL:  Key (id)=(10174) is still referenced from table "ir_actions_server_navigate_line".

# ERREUR n°4 (V13)
# (ANALYSE : à voir si ça disparait pas avec le n°3)
# 2025-02-04 20:47:16,780 8 INFO caap_2025_01_31__TRY_0003 odoo.models.unlink: User #1 deleted ir.model.fields records with IDs: [10254, 11260, 10298, 12130, 12232, 12373, 12572, 12223, 11233, 11261, 10308, 12425, 11270, 10323, 12219, 10246, 12392, 10476, 10466, 10416, 12293, 12422, 12408, 12423, 12340, 12306, 12580, 11269, 12231]
# 2025-02-04 20:47:17,281 8 ERROR caap_2025_01_31__TRY_0003 odoo.sql_db: bad query: DELETE FROM ir_model WHERE id IN (569, 663, 662, 566, 656, 655, 650, 574, 650, 652, 651, 650, 649, 569, 648, 647, 568, 569, 646, 610, 609, 569, 568, 608, 590, 589)
# ERROR: update or delete on table "ir_act_server" violates foreign key constraint "ir_cron_ir_actions_server_id_fkey" on table "ir_cron"
# DETAIL:  Key (id)=(756) is still referenced from table "ir_cron".

# ERREUR n°14-A
# 2025-02-04 21:02:15,785 1 DEBUG caap_2025_01_31__TRY_0003 OpenUpgrade: Deleting record payment.acquirer#14
# 2025-02-04 21:02:15,796 1 INFO caap_2025_01_31__TRY_0003 OpenUpgrade: Error deleting XML-ID payment.payment_acquirer_odoo_by_adyen: UserError("Vous ne pouvez pas supprimer l'acquéreur de paiement Odoo Payments by Adyen ; désactivez-le ou désinstallez-le plutôt.")

# ERREUR n°16-A

# 2025-02-04 21:10:24,553 1 DEBUG caap_2025_01_31__TRY_0003 OpenUpgrade: Deleting record for XML-ID account.data_account_type_other_income
# 2025-02-04 21:10:24,553 1 INFO caap_2025_01_31__TRY_0003 OpenUpgrade: Error deleting XML-ID account.data_account_type_other_income: KeyError('account.account.type')

# 2025-02-04 21:18:51,641 8 ERROR ? odoo.schema: Table 'ir_model_data': unable to add constraint 'ir_model_data_name_nospaces' as CHECK(name NOT LIKE '% %')
# 2025-02-04 21:18:52,248 8 ERROR ? odoo.schema: Table 'mail_channel': unable to add constraint 'mail_channel_group_public_id_check' as CHECK (channel_type = 'channel' OR group_public_id IS NULL)
# 2025-02-04 21:18:54,552 8 ERROR ? odoo.schema: Table 'res_users_role_line': unable to set NOT NULL on column 'user_id'
# 2025-02-04 21:18:54,554 8 ERROR ? odoo.schema: Table 'ir_actions_server_mass_edit_line': unable to set NOT NULL on column 'server_action_id'
# 2025-02-04 21:18:54,555 8 ERROR ? odoo.schema: Table 'uom_category': unable to set NOT NULL on column 'measure_type'
# 2025-02-04 21:18:54,585 8 ERROR ? odoo.schema: Table 'account_tax': unable to set NOT NULL on column 'country_id'


# TODO, delete queue_job
