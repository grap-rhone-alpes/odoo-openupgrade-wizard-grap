#!/bin/bash

set -ex

cd src/env_12.0/
otg diff -c ./repo_submodule/repos.yml
cd  ../env_13.0/
otg diff
cd  ../env_14.0/
otg diff
cd  ../env_15.0/
otg diff
cd  ../env_16.0/
otg diff -c ./repo_submodule/repos.yml
