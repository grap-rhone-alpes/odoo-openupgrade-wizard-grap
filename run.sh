#!/bin/bash

# odoo-openupgrade-wizard init --initial-release 12.0 --final-release 14.0 --project-name my-customer-12-14 --extra-repository OCA/web,OCA/server-tools

# odoo-openupgrade-wizard docker-build
db_name="test_migration_12_14__3"


odoo-openupgrade-wizard run\
    --step 1\
    --database $db_name\
    --init-modules=product,web_responsive\
    --stop-after-init

# optional
# odoo-openupgrade-wizard run --step 1 --database $db_name


odoo-openupgrade-wizard upgrade --database $db_name

odoo-openupgrade-wizard run --step 4 --database $db_name
