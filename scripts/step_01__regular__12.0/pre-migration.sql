-- synchronization module is bad designed. It is not possible to
-- to delete ir.model if there are related to a synchronization_data item.
-- so we hardly truncate that table
TRUNCATE synchronization_data CASCADE;

-- Remove useless done / failed jobs
delete from queue_job where state in ('done', 'failed');

-- Reset login of admin user
UPDATE res_users
SET login = 'admin@grap.coop'
WHERE id in (
    SELECT res_id from ir_model_data
    WHERE model = 'res.users'
    AND name = 'user_admin'
);

-- Give access to all companies for admin user
INSERT INTO res_company_users_rel (
select
    id as cid,
    (select res_id from ir_model_data where model = 'res.users' and name='user_admin' limit 1) as user_id
FROM res_company rc
WHERE rc.id not in (
    SELECT cid
    FROM res_company_users_rel
    WHERE user_id = (
        select res_id from ir_model_data where model = 'res.users' and name='user_admin' limit 1
    )
)
order by rc.id);

-- Set default company to ALL, for admin user.
UPDATE res_users
SET company_id = (select id from res_company where code = 'ALL')

where id = (
        select res_id from ir_model_data where model = 'res.users' and name='user_admin' limit 1
    );


-- Prepare database_synchronization uninstallation
DELETE FROM queue_job_function WHERE channel ilike '%database_synchronization_install_module%';

--delete from queue_job_channel where name ilike '%database_synchronization_install_module%';
