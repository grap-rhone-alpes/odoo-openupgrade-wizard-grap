# Suppression de toutes ls actions de navigations.
# (certaines pointes vers des modèles qui disparaissent
# comme (account.invoice).
# autant refaire tout clean après.
delete * from ir_actions_server_navigate_line;
delete from ir_act_server where state = 'navigate';
