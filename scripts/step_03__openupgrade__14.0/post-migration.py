# ##############################################################################################
# <COMMON>
# ##############################################################################################
import logging

_logger = logging.getLogger("[OOW]")
_logger.info("==========================================")
_logger.info("Execution of post-migration.py ...")
_logger.info("==========================================")


env = env  # noqa: F821


def _get_module_states(module_dict, step):
    for item in env["ir.module.module"].search_read([], ["name", "state"]):
        if item["name"] in module_dict:
            module_dict[item["name"]][step] = item["state"]
        else:
            module_dict[item["name"]] = {step: item["state"]}


def _show_differences(module_dict, module_list):
    for module_name, states in module_dict.items():
        if (
            states.get("before", "uninstalled") != states.get("after", "uninstalled")
            and module_name not in module_list
        ):
            _logger.error(
                f"Module {module_name}. State change From '{states.get('before', 'uninstalled')}' to '{states.get('after', 'uninstalled')}'."
            )


def _install_modules(module_list):
    module_dict = {}
    _get_module_states(module_dict, "before")

    _logger.info(f"Installing {len(module_list)} modules ...")
    for i, module_name in enumerate(module_list, 1):
        module = env["ir.module.module"].search([("name", "=", module_name)])
        if module.state == "installed":
            if module_name in [
                x for x, v in module_dict.items() if v["before"] == "installed"
            ]:
                _logger.error(
                    f"{i}/{len(module_list)} module '{module_name}' was already installed before the script was run."
                )
            else:
                # the module has been installed by dependency, not a big deal.
                _logger.info(
                    f"{i}/{len(module_list)} module '{module_name}' is already installed."
                )
        else:
            _logger.info(
                f"{i}/{len(module_list)} Installing module '{module_name}' ..."
            )
            module.button_immediate_install()
            _logger.info(f"module '{module_name}' installed.")

    _get_module_states(module_dict, "after")

    _show_differences(module_dict, module_list)


def _uninstall_modules(module_list):
    module_dict = {}
    _get_module_states(module_dict, "before")

    _logger.info(f"Uninstalling {len(module_list)} modules ...")
    for i, module_name in enumerate(module_list, 1):
        module = env["ir.module.module"].search([("name", "=", module_name)])
        if module.state == "uninstalled":
            if module_name in [
                x for x, v in module_dict.items() if v["before"] == "uninstalled"
            ]:
                _logger.error(
                    f"{i}/{len(module_list)} module '{module_name}' was already uninstalled before the script was run."
                )
            else:
                # the module has been uninstalled by dependency, not a big deal.
                _logger.info(
                    f"{i}/{len(module_list)} module '{module_name}' is already uninstalled."
                )
        else:
            _logger.info(
                f"{i}/{len(module_list)} Uninstalling module '{module_name}' ..."
            )
            module.button_immediate_uninstall()
            _logger.info(f"module '{module_name}' uninstalled.")

    _get_module_states(module_dict, "after")

    _show_differences(module_dict, module_list)


# ##############################################################################################
# </COMMON>
# ##############################################################################################

# Write custom script here
_UNINSTALL_MODULES = [
    "mass_operation_abstract",
]

# 1) Uninstall Modules
_uninstall_modules(_UNINSTALL_MODULES)


env.cr.commit()
