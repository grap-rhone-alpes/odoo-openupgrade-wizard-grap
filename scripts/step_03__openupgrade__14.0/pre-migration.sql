-- Audacieux, on supprime tous les bank statement qui sont relié au PdV.
-- en V13, on a créé tous les pos.payment depuis les bank statement line.
-- on considère qu'à partir de V14+ ils servent à rien, vu que le comportement en V16 c'est:
-- pour le journal de type cash, lors de la fermeture, ajoute un (des?) statement lines
-- à un statement existant
-- pour les journaux de type bank, ne génère aucun statement line.

-- Cela évite d'échouer, lors de l'étape de post migration de account,
-- quand il y a plein de bank_statement_line qui sont sans move_id.
-- ce qui est dû à l'usage en V12, du module grap_pos_change_payment_move
-- qui créé des écritures synthétiques.
delete from  account_bank_statement where pos_session_id is not null;
