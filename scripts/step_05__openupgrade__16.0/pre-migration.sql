-- Delete all views, to force to recreate them, avoiding
-- to fix a lot of messy errors.

-- avoid to delete qweb view, because they don't generate errors AND are related to other model
-- in a required manner. (like product_print_category)
DELETE FROM ir_ui_view WHERE type != 'qweb';

TRUNCATE ir_model_access;

DELETE FROM ir_rule;
