# ##############################################################################################
# <COMMON>
# ##############################################################################################
import logging

_logger = logging.getLogger("[OOW]")
_logger.info("==========================================")
_logger.info("Execution of post-migration.py ...")
_logger.info("==========================================")


env = env  # noqa: F821


def _get_module_states(module_dict, step):
    for item in env["ir.module.module"].search_read([], ["name", "state"]):
        if item["name"] in module_dict:
            module_dict[item["name"]][step] = item["state"]
        else:
            module_dict[item["name"]] = {step: item["state"]}


def _show_differences(module_dict, module_list):
    for module_name, states in module_dict.items():
        if (
            states.get("before", "uninstalled") != states.get("after", "uninstalled")
            and module_name not in module_list
        ):
            _logger.error(
                f"Module {module_name}. State change From '{states.get('before', 'uninstalled')}' to '{states.get('after', 'uninstalled')}'."
            )


def _install_modules(module_list):
    module_dict = {}
    _get_module_states(module_dict, "before")

    _logger.info(f"Installing {len(module_list)} modules ...")
    for i, module_name in enumerate(module_list, 1):
        module = env["ir.module.module"].search([("name", "=", module_name)])
        if module.state == "installed":
            if module_name in [
                x for x, v in module_dict.items() if v["before"] == "installed"
            ]:
                _logger.error(
                    f"{i}/{len(module_list)} module '{module_name}' was already installed before the script was run."
                )
            else:
                # the module has been installed by dependency, not a big deal.
                _logger.info(
                    f"{i}/{len(module_list)} module '{module_name}' is already installed."
                )
        else:
            _logger.info(
                f"{i}/{len(module_list)} Installing module '{module_name}' ..."
            )
            module.button_immediate_install()
            _logger.info(f"module '{module_name}' installed.")

    _get_module_states(module_dict, "after")

    _show_differences(module_dict, module_list)


def _uninstall_modules(module_list):
    module_dict = {}
    _get_module_states(module_dict, "before")

    _logger.info(f"Uninstalling {len(module_list)} modules ...")
    for i, module_name in enumerate(module_list, 1):
        module = env["ir.module.module"].search([("name", "=", module_name)])
        if module.state == "uninstalled":
            if module_name in [
                x for x, v in module_dict.items() if v["before"] == "uninstalled"
            ]:
                _logger.error(
                    f"{i}/{len(module_list)} module '{module_name}' was already uninstalled before the script was run."
                )
            else:
                # the module has been uninstalled by dependency, not a big deal.
                _logger.info(
                    f"{i}/{len(module_list)} module '{module_name}' is already uninstalled."
                )
        else:
            _logger.info(
                f"{i}/{len(module_list)} Uninstalling module '{module_name}' ..."
            )
            module.button_immediate_uninstall()
            _logger.info(f"module '{module_name}' uninstalled.")

    _get_module_states(module_dict, "after")

    _show_differences(module_dict, module_list)


# ##############################################################################################
# </COMMON>
# ##############################################################################################

# Write custom script here
_UNINSTALL_MODULES = [
    "base_install_request",
    "pos_payment_terminal",
    # replaced by crm_tag_multi_company / crm_stage_multi_company / crm_lost_reason_multi_company
    "multi_company_crm",
]
_INSTALL_MODULES = [
    # 3 modules Replace multi_company_crm
    "crm_lost_reason_multi_company",
    "crm_tag_multi_company",
    "crm_stage_multi_company",
    "res_company_access_all_childs",
    # new technical_partner_access.
    # (complement of partner_hide_technical_company)
    "partner_hide_technical_user",
    "pos_discount_all",
    "mail_discuss_security",
    "base_name_search_improved",
    "pos_payment_description",
    "pos_driver_payment",
    "pos_driver_device_list",
    "account_accountant_simple_settings",
    "account_menu_invoice_refund",
    "account_sequence",
    "create_recursive_abstract",
    "create_recursive_pos_category",
    "create_recursive_product_category",
    "date_range_account",
    "l10n_fr_pos_cert_update_draft_order_line",
    "l10n_fr_invoice_addr",
    "hr_contract",
    # following 3 modules are installed by grap_cooperative / fermente_cooperative_directory
    # "hr",
    # "hr_employee_firstname",
    # "hr_employee_firstname_partner_firstname",
    "hr_expense",
    "fermente_base",
    "fermente_account",
    # following 1 module, installed by grap_account_export
    # "fermente_account_export",
    "fermente_account_invoice_margin",
    "fermente_account_invoice_triple_discount",
    "fermente_account_menu_invoice_refund",
    "fermente_account_move_name_sequence",
    "fermente_custom_import_account_product_fiscal_classification",
    "fermente_custom_import_base",
    "fermente_custom_import_partner_firstname",
    "fermente_custom_import_product",
    "fermente_custom_import_product_label",
    "fermente_custom_import_product_margin_classification",
    "fermente_custom_import_product_supplierinfo_qty_multiplier",
    "fermente_custom_import_purchase_discount",
    "fermente_custom_import_purchase_triple_discount",
    "fermente_hr",
    "fermente_main_menu",
    "fermente_mrp",
    "fermente_mrp_bom_form_view",
    "fermente_pos",
    "fermente_product",
    "fermente_product_category_active",
    "fermente_product_margin_classification",
    "fermente_stock",
    "fermente_web_environment_ribbon",
    "pos_category_complete_name",
    "pos_check_accounting_settings",
    "pos_hr",
    "pos_hr_restaurant",
    "pos_minimize_menu",
    "pos_mrp",
    "pos_order_new_line",
    "pos_partner_firstname",
    "pos_payment_usability",
    "pos_receipt_hide_info",
    "pos_receipt_usability",
    "pos_receipt_vat_detail",
    "pos_restaurant_receipt_usability",
    "pos_sale_margin",
    "pos_sale_product_configurator",
    "product_accounts",
    "product_category_global_account_setting",
    "product_company_default",
    "product_food_certification_stock",
    "product_label_mrp",
    "product_pricelist_simulation_margin",
    "product_standard_price_change_date",
    "project_tag_hierarchy",
    "project_tag_multicompany",
    "project_tag_security",
    "sale_expense",
    "sale_expense_margin",
    "web_select_only_child_company",
    "web_theme_classic",
]

_INSTALL_MODULES += [
    # TODO, FIXME, pourquoi je dois l'installer, alors que
    # technical_partner_access était installé ?!?
    "partner_hide_technical_company",
]

if env.cr.dbname.startswith("caap_"):
    _logger.info("[CAAP] Adding Extra modules to uninstall...")
    _UNINSTALL_MODULES += [
        "intercompany_trade_joint_buying_base",
        "joint_buying_purchase",
        "joint_buying_sale",
        "joint_buying_stock",
        "joint_buying_account",
        "joint_buying_product",
        "joint_buying_product_food",
        "joint_buying_product_label",
        "joint_buying_base",
    ]

# 1) Install Modules
_install_modules(_INSTALL_MODULES)

# 2) TODO: makes migration from pos_payment_terminal to pos_driver_payment

# 3) Uninstall Modules
_uninstall_modules(_UNINSTALL_MODULES)

env.cr.commit()
