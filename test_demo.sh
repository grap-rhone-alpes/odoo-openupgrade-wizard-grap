DB_NAME=test_demo_2024_10_11

oow install-from-csv -d $DB_NAME

oow dumpdb -d $DB_NAME\
    --database-path ./data_for_test/$DB_NAME.dump\
    --filestore-path ./data_for_test/DB_NAME.tgz\
    --database-format c\
    --filestore-format tgz

oow copydb -s $DB_NAME -d $(DB_NAME)_TEST

oow execute-script-python -d $DB_NAME -s 1
