#!/bin/bash

# TODO: Download a new modules.csv file

# Git pull Origin 12.0 repos.yml
cd src/env_12.0/repo_submodule/
git pull origin 12.0
cd ../../../

# Git pull Origin 12.0 repos.yml
cd src/env_16.0/repo_submodule/
git pull origin 16.0
cd ../../../

# . ../odoo-openupgrade-wizard/env/bin/activate

# Get Code
oow get-code --versions=12.0,13.0,14.0,15.0,16.0

oow --log-level=DEBUG estimate-workload
